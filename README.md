StreamDesk-old
==============
This repo contains SD Source Code for versions 2.2.1 and below. To access a specific version of source code
execute the following

```
git checkout v<SD Version Number>
```

If you want current source code, see the other projects on GitLab
